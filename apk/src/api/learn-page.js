const http = uni.$u.http

export function GetChapterDetail(chapter_id,chapters_id){
    return http.get(`/resource/${chapter_id}/${chapters_id}/chapters.json`+'?t=' +Math.random())
}